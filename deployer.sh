#!bin/bash

# Comprobamos usuario root

if [ $UID -ne 0 ]; then

        echo "Ejecute este script como 'root' o 'sudo ./$0'"
        exit 1
fi

# Actualizamos el sistema
apt update && apt upgrade -y || yum update || dnf update


# Hostname
read -p "hosname: " host
hostnamectl set-hostname $host

# Instalar nginx
apt install ngnix -y || yum install nginx || dnf install ngnix




